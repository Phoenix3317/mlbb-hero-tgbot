import { Bot, webhookCallback } from "grammy";

const token = process.env.TOKEN
if (!token) throw new Error("TOKEN required!")

const bot = new Bot(token)

bot.command('start', ctx => {
    if (!ctx.from?.is_bot)
    {
        let username = ctx.from?.first_name + ' ' + ctx.from?.last_name
        let withlink = `[${username}](https:t.me//${ctx.from?.username})`

        ctx.reply('Welcome, ' + withlink)
    }
})

bot.on('message', ctx => {
    if (ctx.message.text == 'ping') ctx.reply('pong')
    else {
        ctx.reply('Your message: ' + ctx.message.text)
    }
})

export default webhookCallback(bot, 'http')